import React from 'react';
import Dashboard from './pages/Dashboard'
import Header from './pages/Header'
import Transfer from './pages/Transfer'
import {  } from 'react-router-dom'
function App() {
  return (
      <div>
        <Header />
        <Dashboard />
      </div>
  );
}

export default App;
