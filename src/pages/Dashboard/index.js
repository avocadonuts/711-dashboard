import React, { Component } from 'react'
import {
  Container,
  Row,
  Col,
  Button
} from 'react-bootstrap'
import Card from '../../components/Card'
import UserInfo from '../../components/UserInfo'
// import App from '../../components/ModalTransfer'


class Dashboard extends Component {  
  constructor(props){
    super(props);
    this.state = {
      flipped: 'true',
    };
    this.flipCard = this.flipCard.bind(this);
  }
  flipCard(){
    const flipped = !this.state.flipped
    this.setState({ flipped })
  }
    render() {  
        return (
            <div style={{height: "87vh", background: "gray"}}>
              <Container>
                <Row>
                  <Col sm={4} style={{ height: "100%", marginTop: "100px"}}><Card name="Alvin Geronim0" number="1239873" flipped={this.state.flipped} /></Col>
                  <Col sm={8} style={{ height: "100%", marginTop: "100px", paddingLeft: "100px"}}><UserInfo /></Col>
                  <button onClick={this.flipCard}></button>
                </Row>
              </Container>
            </div>
        )
    }
}

export default Dashboard;
