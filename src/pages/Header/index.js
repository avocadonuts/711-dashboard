import React, { Component } from 'react'
import ModalButton from '../../components/ModalTransfer'
import {
  Navbar,
  Nav,
  Form,
  FormControl,
  Button,
  Row,
  Col
} from 'react-bootstrap'

class Header extends Component {
  render() {
    return (
        <div>
          <Navbar expand="lg" variant="dark" style={{height: "100px", background:"#505356"}}>
            <Navbar.Brand style={{marginLeft: "5%"}} href="#home">Navbar</Navbar.Brand>
            <Nav className="mr-auto">
                <Nav.Link></Nav.Link>
            </Nav>
            <Form style={{marginRight: "5%"}}>
              <Nav className="mr-auto" style={{ left: "200px" }}>
                  <Nav.Link></Nav.Link>
                  <ModalButton />
              </Nav>
            </Form>
          </Navbar>
        </div>
      )
   }
}

export default Header;