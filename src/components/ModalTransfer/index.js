import React, { Component } from 'react'
import {
  Modal,
  Button,
  ButtonToolbar,
  Form,
  Row,
  Col
} from 'react-bootstrap'

// const ModalTransfer = (props) => {
//   return (
//     <Modal
//       size="lg"
//       aria-labelledby="contained-modal-title-vcenter"
//       centered>
//       <Modal.Header closeButton>
//         <Modal.Title> Transfer Coin</Modal.Title>
//       </Modal.Header>
//       <Modal.Body>
//         <h4>My Coins</h4>
//       </Modal.Body>
//       <Modal.Footer>
//         <Button onClick={props.onHide}>Close</Button>
//       </Modal.Footer>
//     </Modal>
//   ) 
// } 



class ModalButton extends Component {
  constructor(props) {
    super(props);
    this.state = {
      show: "false",
    }
    this.toggleModal = this.toggleModal.bind(this);
  }

  toggleModal() {
    const show = !this.state.show
    this.setState({ show })
  }

  render() {
    return (
      <ButtonToolbar>
        <Button variant="light" onClick={this.toggleModal}>
          Transfer
        </Button>
  
        {/* <ModalTransfer
         show={this.state.show}
        /> */}

  <Modal
      show={this.state.show}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered>
      <Modal.Header closeButton>
        <Modal.Title> Transfer Coin</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <h4>My Coins</h4>
        <Form.Group as={Row} controlId="formPlaintextPassword" style={{ marginTop: '30px'}}>
          <Form.Label column sm="2" style={{textAlign: "right"}}>
            <h4>Mint</h4>
          </Form.Label>
          <Col sm="8">
            <Form.Control type="number" placeholder="Amount" />
          </Col>
          <Col sm="2">
            <Button>Send</Button>
          </Col>
        </Form.Group>
      </Modal.Body>
      <Modal.Footer>
        <Button onClick={this.toggleModal}>Close</Button>
      </Modal.Footer>
      </Modal>
      </ButtonToolbar>
    );
  }
}


export default ModalButton; 