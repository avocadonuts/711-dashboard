import React, { Component } from 'react'
import PaymentCard from 'react-payment-card-component'
import {
 Image,
 Button,
} from 'react-bootstrap'

import confirmed from "../../shield.png";
const Card = ({ name, number, brand, bank, flipped }) => {
  return (
    <div style={{}}>
      <div style={{}}>
      <PaymentCard
        bank={bank}
        model="Ourocard"
        type="black"
        brand={brand}
        number={number}
        cvv="202"
        holderName={name}
        expiration="12/20"
        flipped={flipped}
      />
      <div style={{textAlign: "center"}}>
        <Image src={confirmed} roundedCircle style={{height: "150 px", width: "150px", marginTop: "50px"}} />
        <div style={{color: "#53fc86", marginTop: "20px", fontSize: '2em', letterSpacing: ".1em",fontWeight: "5em" }}><h3>VERIFIED</h3></div>
      </div>
      </div>
    </div>
  )
}

export default Card;