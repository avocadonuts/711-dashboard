import React, { Component } from 'react'
import {
  Card,
  Alert,
  Form,
  Row,
  Col
} from 'react-bootstrap'
const UserInfo = ({fullname, address, email, sss}) => {
  return (
    <div>
      <Card bg="dark" text="light">
        <Card.Body style={{fontSize: "1em", padding: '40px'}}>
          {/* <Alert style={{background: "#15222f", color: "#fff"}}> */}
            <div style={{display: 'flex', fontSize: "1.2em", color:"#b9b9b9"}}>
              <Card.Text style={{ marginRight: "10px"}}>Fullname:</Card.Text>
              <Card.Text style={{color:"#fff"}}>Alvin Geronimo</Card.Text>
            </div>
            <div style={{display: 'flex', fontSize: "1.2em", color:"#b9b9b9"}}>
              <Card.Text style={{ marginRight: "10px"}}>Address:</Card.Text>
              <Card.Text style={{color:"#fff"}}>Shaw House</Card.Text>
            </div>
            <div style={{display: 'flex', fontSize: "1.2em", color:"#b9b9b9"}}>
              <Card.Text style={{ marginRight: "10px"}}>Email:</Card.Text>
              <Card.Text style={{color:"#fff"}}>alvins@gmail.com</Card.Text>
            </div>
            <div style={{display: 'flex', fontSize: "1.2em", color:"#b9b9b9"}}>
              <Card.Text style={{ marginRight: "10px"}}>SSS Account no.:</Card.Text>
              <Card.Text style={{color:"#fff"}}>98209321 920839871203 902739 -2</Card.Text>
            </div>
          {/* </Alert> */}
        </Card.Body>
      </Card>
    </div>
  )
}

export default UserInfo;